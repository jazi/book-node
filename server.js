//ENV Variabels
if(process.env.NODE_ENV !== 'production'){
    require('dotenv').config();
}
   
const express = require('express');
const app = express();
const expressLayouts = require('express-ejs-layouts');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');

//Add Routers
const indexRouter = require('./routes/index');
const authorsRouter = require('./routes/authors');
const booksRouter = require('./routes/books');

//Set Views & Layouts
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.set('layout', 'layouts/layout');
//ADD librarys to this project
app.use(expressLayouts);
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ limit:'10mb' , extended:false }));
app.use(methodOverride('_method'));

//Config Database
const mongoose = require('mongoose');
mongoose.connect(process.env.DATABASE_URL , {useNewUrlParser : true , useUnifiedTopology: true});
const db = mongoose.connection ;
db.on('err' , error => console.error(error));
db.once('open' , () => console.log('Connect to MongoDB'));

//Config Routers
app.use('/', indexRouter);
app.use('/authors', authorsRouter);
app.use('/books', booksRouter);

//Listen To Ports
app.listen(process.env.PORT || 3000);