const express = require('express');
const router = express.Router();
const Book = require('../models/book');

/*
//with callback
router.get('/', (req, res) => {
    Book.find({} , (err , books)=>{
        if(err){
            console.log('Home Page Error '+err);
            res.render('index' , {books:[]});
        }
        else{
            res.render('index' , {books:books});
        }

    }).sort({createdAt : 'desc'}).limit(10).exec();
});
*/

//with async await
router.get('/', async (req, res) => {
    let books ;
    try{
        books = await Book.find({}).sort({createdAt : 'desc'}).limit(10).exec();
    }
    catch{
        books = [];
    }
    res.render('index' , {books:books});
});
 

module.exports = router