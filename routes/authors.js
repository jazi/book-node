const express = require('express');
const router = express.Router();
//Import Models
const Author = require('../models/author');
const Book = require('../models/book');
//Display All Authors
router.get('/', async (req, res) => {
    let searchOptions = {} ;
    if(req.query.name != null && req.query.name !== ''){
        searchOptions.name = new RegExp(req.query.name.trim , 'i');
    }
    try{
        const authors = await Author.find(searchOptions);
        res.render('authors/index' , {authors : authors , searchOptions : searchOptions});
    }
    catch{
        res.redirect('/');
    }
});

//Display a New Authors
router.get('/new' , (req , res)=>{
    res.render('authors/new' , { author:new Author() } );
});

//Create a Author
router.post('/' , async (req , res)=>{
    
    const author = new Author({
        name : req.body.name
    });

    try{
        const newAuthor = await author.save();
        res.redirect(`/authors/${newAuthor.id}`);
    }
    catch{
        res.render('authors/new' , {author:author , errorMessage : 'Error Creating Author'});
    }

    /*
    author.save((err , newAuthor)=>{
        if(err){
            res.render('author/new' , {author:author , errorMessage : 'Error Creatin Author'});
        }
        else {
            res.redirect('authors');
        }
    });
    */

    res.send(req.body.name);
});

//Display One Author
router.get('/:id' , async (req , res)=>{
    try{
        const author = await Author.findById(req.params.id);
        const books = await Book.find({author : author.id}).limit(6).exec();
        res.render('authors/show' , {author : author , booksByAuthor : books});
    }
    catch(err){
        console.log(err);
        res.redirect('/');
    }
});

router.get('/:id/edit' , async (req , res)=>{
    try {
        const author = await Author.findById(req.params.id);
        res.render('authors/edit' , {author : author});
    } 
    catch {
        res.redirect('/authors');
    }
});

router.put('/:id' , async(req , res)=>{
    
    let author ;

    try{
        author = await Author.findById(req.params.id);
        author.name = req.body.name ;  //Edit Author
        await author.save() ;
        res.redirect(`/authors/${author.id}`);
    }
    catch{
        if( author == null){
            res.redirect('/');
        }
        else{
            res.render('authors/edit' , {author:author , errorMessage : 'Error Editing Author'});
        }
    }
});

router.delete('/:id' , async (req , res)=>{
    
    let author ;

    try{
        author = await Author.findById(req.params.id);
        console.log("FIND FOR DELETE");
        await author.remove() ;
        res.redirect('/authors');
    }
    catch{
        if( author == null){
            res.redirect('/');
        }
        else{
            res.redirect(`/authors/${author.id}`);
        }
    }
});


module.exports = router